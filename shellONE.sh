#!/bin/bash
sudo apt-get update --fix-missing
sudo apt-get install -y wget bzip2 ca-certificates \
libglib2.0-0 libxext6 libsm6 libxrender1 \
git mercurial subversion

sudo echo 'export PATH=/opt/conda/bin:$PATH' | sudo tee /etc/profile.d/conda.sh
sudo wget --no-check-certificate -P /tmp https://repo.continuum.io/archive/Anaconda3-2.5.0-Linux-x86_64.sh
sudo /bin/bash /tmp/Anaconda3-2.5.0-Linux-x86_64.sh -b -p /opt/conda
sudo rm /tmp/Anaconda3-2.5.0-Linux-x86_64.sh

sudo apt-get install -y curl grep sed dpkg
sudo mkdir -p /etc/pki/tls/certs
sudo ln -s /etc/ssl/certs/ca-certificates.crt /etc/pki/tls/certs/ca-bundle.crt
TINI_VERSION=`curl https://github.com/krallin/tini/releases/latest | grep -o "/v.*\"" | sed 's:^..\(.*\).$:\1:'`
sudo curl -L "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini_${TINI_VERSION}.deb" > /tmp/tini.deb
sudo dpkg -i /tmp/tini.deb
sudo rm /tmp/tini.deb
sudo apt-get clean

export PATH=/opt/conda/bin:$PATH.
export LANG=C.UTF-8

sudo apt-get install -y python-software-properties
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu trusty main"
sudo apt-get install debian-archive-keyring 
sudo apt-key update
sudo apt-get update
sudo apt-get install --force-yes -y ubuntu-keyring
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 40976EAF437D05B5 3B4FE6ACC0B21F32
sudo mv /var/lib/apt/lists /tmp 
sudo mkdir -p /var/lib/apt/lists/partial
sudo apt-get clean 
sudo apt-get update
sudo apt-get install -y g++-4.8
    
sudo /opt/conda/bin/conda install anaconda python=3.5 -y
jupyter notebook --pylab=inline --ip=0.0.0.0 --no-browser &
