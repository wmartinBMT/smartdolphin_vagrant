#!/bin/bash
sudo apt-get update

sudo apt-get -y install python-pip python-dev build-essential 
sudo apt-get -y install libzmq-dev
sudo pip install --upgrade pip 
sudo pip install --upgrade virtualenv
#sudo apt-get -y install numpy
sudo apt-get -y install python-matplotlib python-scipy python-pandas python-sympy python-nose
sudo pip install pyzmq jinja2 tornado
sudo pip install ipython
